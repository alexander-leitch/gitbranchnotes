New Empty Branch
================

$ git checkout --orphan NEWBRANCH // creates a new branch without history.

$ git rm -rf . // Removes files from other branch

New Branch
==========

$ git checkout -b NEWBRANCH //Switched to a new branch "NEWBRANCH"

OR

$ git branch NEWBRANCH

$ git checkout NEWBRANCH

Merge Branches
==============

$ git checkout master // Go into branch

$ git merge hotfix // Merge hotfix into master

Push all branches to remote server
==================================

$ git push -u origin --all

Delete a branch
===============

$ git branch -D hotfix


Delete a remote branch
======================

$ git push origin --delete <branchName>

or

$ git push origin :<branchName>

Resources
=================
[http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging](http://git-scm.com/book/en/Git-Branching-Basic-Branching-and-Merging)
[http://www.bitflop.com/document/116](http://www.bitflop.com/document/116)

[http://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-both-locally-and-in-github](http://stackoverflow.com/questions/2003505/how-do-i-delete-a-git-branch-both-locally-and-in-github)